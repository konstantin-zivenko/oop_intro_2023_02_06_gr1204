# Створіть клас, який описує книгу. Він повинен містити інформацію про автора,
# назву, рік видання та жанрі. Створіть кілька книжок. Визначте для нього
# операції перевірки на рівність та нерівність, методи __repr__ та __str__.


class Book:
    def __init__(
            self,
            author: str,
            title: str,
            published: int,
            genre: str
            ) -> None:
        self.author = author
        self.title = title.capitalize()
        self.published = published
        self.genre = genre


    def __repr__(self) -> str:
        return f"Book({self.author}, {self.title}, {self.published}, {self.genre})"


    def __str__(self) -> str:
        return f"it is Book\nname: {self.title}\nauthor: {self.author}"


    def __eq__(self, __value: object) -> bool:
        if not isinstance(__value, Book):
            raise TypeError("------")
        return self.title == __value.title and self.author == __value.author
   

if __name__ == "__main__":
    book_1 = Book(
            "Azimov",
            "Foundation",
            1960,
            "SF"
            )
        
    book_2 = Book(
            "Azimov",
            "Foundation",
            1972,
            "SF"
            )
    print(book_1)        
    print(book_2)
 


    print(f"{book_1 == book_2}")
